#define USE_TYPED_DSET

#include <string>
#include <cstdio>
#include <cstring>

#include <epicsMath.h>
#include <dbAccess.h>
#include <dbScan.h>
#include <dbLink.h>
#include <recGbl.h>
#include <alarm.h>
#include <cantProceed.h>
#include <errlog.h>

#include <inttypes.h>

#include <longinRecord.h>
#include <int64outRecord.h>
#include <epicsExport.h>

//global variable that holds UTAG
static epicsUInt64 gblUTAG;

long init_li_rec(longinRecord *prec) {
    int err = -1;

    if (prec->inp.value.constantStr) {
        
        // Checks if INP is one of the valid options
        const std::string iolink(prec->inp.value.constantStr);
    
        // set value to zero
        if (iolink == "timeRef") {
            prec->val = 0;
            err = 0;
        }   
    } 

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s failed to initialize\n", prec->name);
    }
    return static_cast<long>(err);
}


long process_li_rec(longinRecord *prec) {
    int err = -1;
    const std::string iolink(prec->inp.value.constantStr);

    // Checks if record has private structure
    if (iolink == "timeRef") {

        // Set UTAG 
        prec->utag = gblUTAG;

        // Increase counter
        prec->val += 1;
        err = 0;
    } 

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s is invalid\n", prec->name);
    }

    return static_cast<long>(err);
}

long init_int64(int64outRecord *prec) {
    int err = -1;

    if (prec->out.value.constantStr) {
        
        // Checks if OUT is one of the valid options
        const std::string iolink(prec->out.value.constantStr);
    
        // point dpvt to global variable
        if (iolink == "UtagSink") {
            prec->dpvt = static_cast<void *>(&gblUTAG);
            err = 0;
        }   
    } 

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s failed to initialize\n", prec->name);
    }
    return static_cast<long>(err);
}

long process_int64(int64outRecord *prec) {
    int err = -1;
    const std::string iolink(prec->out.value.constantStr);

    // Checks if record has private structure
    if ((prec->dpvt) and (iolink == "UtagSink")) {

        // Just save value in the private structure
        gblUTAG = static_cast<epicsUInt64>(prec->val);

        // Set UTAG 
        prec->utag = gblUTAG;
        err = 0;
    } 

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s is invalid\n", prec->name);
    }

    return static_cast<long>(err);
}


template<typename REC>
struct dset5 {
    long count;
    long (*report)(int);
    long (*init)(int);
    long (*init_record)(REC *);
    long (*get_ioint_info)(int, REC *, IOSCANPVT*);
    long (*process)(REC *);
};

dset5<longinRecord> devLITimeRefRec = {5,0,0,&init_li_rec,0,&process_li_rec};
dset5<int64outRecord> devInt64TimeRefRec = {5,0,0,&init_int64,0,&process_int64};

extern "C" {
epicsExportAddress(dset, devLITimeRefRec);
epicsExportAddress(dset, devInt64TimeRefRec);
}
