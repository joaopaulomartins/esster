#pragma once

#include <epicsTypes.h>
#include <epicsMutex.h>
#include <epicsGuard.h>
#include <shareLib.h>

// UTag  bit mapping
#define ESSTER_UTAG_PULSEID_MASK 0xFFFFFFFF
#define ESSTER_UTAG_PULSEID_OFFS 0

#define ESSTER_UTAG_BEAMMODE_MASK 0xFF
#define ESSTER_UTAG_BEAMMODE_OFFS 32

#define ESSTER_UTAG_BEAMDEST_MASK 0xFF
#define ESSTER_UTAG_BEAMDEST_OFFS 40

#define ESSTER_UTAG_BEAMPRES_MASK 0x01
#define ESSTER_UTAG_BEAMPRES_OFFS 48

#define MASKSHIFT64(val,mask,offs) (static_cast<epicsUTag>(val) & mask) << offs

typedef epicsGuard<epicsMutex> Guard;
typedef epicsGuardRelease<epicsMutex> UnGuard;

// Global structure of data managed by the devsup
typedef struct essterCommon
{
    epicsInt32 pulseId;
    epicsInt32 beamPresent;
    epicsInt32 beamDest;
    epicsInt32 beamMode;
    epicsInt32 tsSecDly;
    epicsInt32 tsNSecDly;
    epicsInt32 tsSecs;
    epicsInt32 tsNSecs;
    //mutable epicsMutex lock;
} essterCommon;

static essterCommon gblEssRefData = {
    0,
    1,
    0,
    0,
    0,
    0
};

epicsShareFunc essterCommon *pgblEssRefData = &gblEssRefData;

typedef struct essterPriv
{
    essterCommon *gblRefData;
    char paramName[16u];
    epicsInt32 value;
} essterPriv;
