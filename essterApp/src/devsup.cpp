#define USE_TYPED_DSET

#include <string>
#include <cstdio>
#include <cstring>

#include <epicsMath.h>
#include <dbAccess.h>
#include <dbScan.h>
#include <dbLink.h>
#include <recGbl.h>
#include <alarm.h>
#include <cantProceed.h>
#include <errlog.h>

#include <inttypes.h>

#include <longinRecord.h>
#include <longoutRecord.h>
#include <stringoutRecord.h>
#include <epicsExport.h>

#include "essterCommon.h"

template <typename REC>
int initPrivate(REC *prec, const std::string& param) {
    if ((param == "pulseID") or (param == "beamPresent") or (param == "beamMode") \
        or (param == "beamDest") or (param == "tsSecDly") or (param == "tsNSecDly") \
        or (param == "timeRef") or (param == "tsInput")) {
        
        // Create private data structure tied to the record
        essterPriv *pPvt;
        pPvt = static_cast<essterPriv*>(callocMustSucceed(1, sizeof(*pPvt), "initPrivate pulseId"));
        
        //Copies the string
        strncpy(pPvt->paramName, param.c_str(), (sizeof(pPvt->paramName)-1));

        // Add pointer to common data
        pPvt->gblRefData = pgblEssRefData;

        //assign address of private data to record private structure
        prec->dpvt = pPvt;

    } else {
        errlogPrintf("Param %s is not available for this device support\n",param.c_str());
        return -1;
    }
    return 0;
}

long init_li(longinRecord *prec) {
    int err = 0;

    if (prec->inp.value.constantStr) {
        
        // Checks if INP is one of the valid options
        const std::string iolink(prec->inp.value.constantStr);
        err = initPrivate<longinRecord>(prec, iolink);

        // if param is timeRef, reset value
        if ((!err) and (iolink == "timeRef")) {
            essterPriv *pPvt = static_cast<essterPriv*>(prec->dpvt);
            pPvt->value = 0;
            prec->val = 0;
        }
    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s failed to initialize\n", prec->name);
    }

    return static_cast<long>(err);
}


long process_li(longinRecord *prec) {
    int err = 0;
    
    // Checks if record is the "timeRef" param
    if (prec->dpvt) {
        essterPriv *pPvt = static_cast<essterPriv*>(prec->dpvt);
        const std::string paramNameStr(pPvt->paramName);

        if (paramNameStr == "timeRef") {

            essterCommon *gblData = pPvt->gblRefData;

            // Update the UTAG field
            epicsUTag utag = 0;

#ifdef USERTAG_64BITS
            
            // First 32 bits are pulseId
            utag |= MASKSHIFT64(gblData->pulseId,ESSTER_UTAG_PULSEID_MASK,ESSTER_UTAG_PULSEID_OFFS);

            // Bits 39-32 are Beam Mode
            utag |= MASKSHIFT64(gblData->beamMode,ESSTER_UTAG_BEAMMODE_MASK,ESSTER_UTAG_BEAMMODE_OFFS);

            // Bits 47-40 are Beam Destination
            utag |= MASKSHIFT64(gblData->beamDest,ESSTER_UTAG_BEAMDEST_MASK,ESSTER_UTAG_BEAMDEST_OFFS);

            // Bits 48 is Beam Present
            utag |= MASKSHIFT64(gblData->beamPresent,ESSTER_UTAG_BEAMPRES_MASK,ESSTER_UTAG_BEAMPRES_OFFS);
#else
            // First 16 bits are pulseId
            utag |= (static_cast<epicsUTag>(gblData->pulseId) & 0xFFFF) << 0;

            // Bits 23-16 are Beam Mode
            utag |= (static_cast<epicsUTag>(gblData->beamMode) & 0xFF) << 16;

            // Bits 30-24 are Beam Destination
            utag |= (static_cast<epicsUTag>(gblData->beamDest) & 0x7F) << 24;

            // Bit 31 is Beam Present
            utag |= (static_cast<epicsUTag>(gblData->beamPresent) & 0x01) << 31;

            //Copy the LS 32bits to the MS 32bist to demonstrate the 64 bits transfer
            utag |= (utag & 0xFFFFFFFF) << 32;

#endif
            // Finally set the utag field
            prec->utag = utag;

            // Increase count
            pPvt->value++;
            prec->val = pPvt->value;

            //update timeStamp (adds tsSecDly and tsNSecDly)
            epicsTimeStamp _time;
            _time.secPastEpoch = gblData->tsSecs + gblData->tsSecDly;
            _time.nsec = gblData->tsNSecs + gblData->tsNSecDly;
            prec->time = _time;
        }

    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s is invalid\n", prec->name);
    }

    return static_cast<long>(err);
}


long init_lo(longoutRecord *prec) {
    int err = 0;
    
    if (prec->out.value.constantStr) {       
        // Checks if OUT is one of the valid options
        const std::string iolink(prec->out.value.constantStr);
        err = initPrivate<longoutRecord>(prec, iolink);
    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s failed to initialize\n", prec->name);
    }

    return static_cast<long>(err);
}

long process_lo(longoutRecord *prec) {
    int err = 0;

    // Checks if record has private structure
    if (prec->dpvt) {

        // Get hold of common data
        essterPriv *pPvt = static_cast<essterPriv*>(prec->dpvt);
        essterCommon *gblData = pPvt->gblRefData;

        // Just save value in the private structure
        pPvt->value = static_cast<epicsInt32>(prec->val);

        const std::string paramNameStr(pPvt->paramName);

        // Set parameters
        if (paramNameStr == "pulseID") {
            gblData->pulseId = pPvt->value;
        
        } else if (paramNameStr == "beamPresent") {
            gblData->beamPresent = pPvt->value;
        
        } else if (paramNameStr == "beamMode") {
            gblData->beamMode = pPvt->value;
        
        } else if (paramNameStr == "beamDest") {
            gblData->beamDest = pPvt->value;
        
        } else if (paramNameStr == "tsSecDly") {
            gblData->tsSecDly = pPvt->value;
        
        } else if (paramNameStr == "tsNSecDly") {
            gblData->tsNSecDly = pPvt->value;
        
        } else {
            err = -1;
        }


    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s is invalid\n", prec->name);
    }

    return static_cast<long>(err);
}


long init_so(stringoutRecord *prec) {
    int err = 0;
    
    if (prec->out.value.constantStr) {       
        // Checks if OUT is one of the valid options
        const std::string iolink(prec->out.value.constantStr);
        err = initPrivate<stringoutRecord>(prec, iolink);
    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s failed to initialize\n", prec->name);
    }

    return static_cast<long>(err);
}

long process_so(stringoutRecord *prec) {
    int err = 0;

    // Checks if record has private structure
    if (prec->dpvt) {

        // Get hold of common data
        essterPriv *pPvt = static_cast<essterPriv*>(prec->dpvt);
        const std::string paramNameStr(pPvt->paramName);

        if (paramNameStr == "tsInput") {
            essterCommon *gblData = pPvt->gblRefData;

            epicsUInt32 timeStampSec;
            epicsUInt32 timeStampNsec;
            err = sscanf(prec->val, "%u.%u", &timeStampSec, &timeStampNsec);

            //Parse string value
            if (err == 2) {
                // convert to EPICS time (epoch at 1990)
                timeStampSec -= POSIX_TIME_AT_EPICS_EPOCH;

                // store values in the global data structure
                gblData->tsSecs = timeStampSec;
                gblData->tsNSecs = timeStampNsec;
                err = 0;
            } else {
                err = -1;
            }
        } 

    } else {
        err = -1;
    }

    if (err) {
        recGblSetSevr(prec,LINK_ALARM,INVALID_ALARM);
        prec->pact=1;
        errlogPrintf("Record %s is invalid\n", prec->name);
    }

    return static_cast<long>(err);
}


template<typename REC>
struct dset5 {
    long count;
    long (*report)(int);
    long (*init)(int);
    long (*init_record)(REC *);
    long (*get_ioint_info)(int, REC *, IOSCANPVT*);
    long (*process)(REC *);
};

dset5<longinRecord> devLITimeRef = {5,0,0,&init_li,0,&process_li};
dset5<longoutRecord> devLOTimeRef = {5,0,0,&init_lo,0,&process_lo};
dset5<stringoutRecord> devSOTimeRef = {5,0,0,&init_so,0,&process_so};

extern "C" {
epicsExportAddress(dset, devLITimeRef);
epicsExportAddress(dset, devLOTimeRef);
epicsExportAddress(dset, devSOTimeRef);
}
