# ESSTER - ESS TimE Reference 

Just a simple test with EPICS UTAG field

## ESSTER Device Support

The device support code (`essterApp/src/devsup.cpp`) implements a trivial support for `longin`, `longout` and `stringout` records. It holds a global structure with a few named parameters:

- Pulse ID
- Beam Mode
- Beam Source
- Beam Destination
- Timestamp seconds
- Timestamp nanoseconds

These parameters are written to the device support via EPICS PVs. The device support then encodes the "beam" parameters into a 64 bits word and sets the UTAG field of a reference record with this value. The timestamp fields of the global structure can be the copy of the timestamp of a remote record (via TSEL) and these values will form the timestamp of the reference record. 

The database template for the device support is located in `essterApp/Db/essTimeReference.template`

## Test IOC (internal client)

The basic test IOC (`iocBoot/testIoc/st.cmd`) will create setpoint records that will write its values inside the device support records, enabling the creation of a custom UTAG value. The timestamp source is a counter implemented with a `calc` record.

This is the record that holds the global UTAG and timestamp reference:
```
record(longin, "$(P)$(R)TimeRef") {
    field(DESC, "ESS Pulse Reference Provider")
    field(INP,  "@timeRef")
    field(DTYP, "essTimeRef")
    field(TSE, "-2")
}
```

The test IOC loads a simple client PV that has its TSEL field pointing to the TIME field of the reference record. It can be used to test the transport of UTAG field from one record to another.

```
# Example of common record that gets its TIME and UTAG fields from the reference record
record(calc, "$(P)$(R)Client-ca") {
    field(CALC, "A+1")
    field(INPA, "$(P)$(R)Client-ca")
    field(TSEL, "$(ESSTER)TimeRef.TIME NPP")
    field(SCAN, "1 second")
    field(VAL, "0")
    field(FLNK, "$(P)$(R)Client-ca-UTAG PP")
}
```
## External Client IOC

Inside `iocBoot/clientIoc` there is an IOC (sofIocPVA) that loads a client PV (simple counter) that attempts to obtain its TIME and UTAG fields from an external PV (the `TimeRef`). The idea is to test the transport of TIME and UTAG fields via the network.
 

