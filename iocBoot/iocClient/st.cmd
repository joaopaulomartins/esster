#!../../bin/linux-x86_64-debug/essterIoc

< envPaths

## Load ESSTER receiver
cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/essterIoc.dbd"
essterIoc_registerRecordDeviceDriver pdbbase

#- Set leader and follower prefix
epicsEnvSet("PREFIX", "TEST:FOLLOWER:")
epicsEnvSet("LEADER", "TEST:LEADER:")

epicsEnvSet("UTAG_SOURCE", "$(LEADER)UTAGRef")
epicsEnvSet("TIME_SOURCE", "$(LEADER)TimeRef")
dbLoadRecords("db/essTimeRefReceiver.template", "P=$(PREFIX), R=, TIME_SOURCE=$(TIME_SOURCE), UTAG_SOURCE=$(UTAG_SOURCE)")

cd "${TOP}/iocBoot/${IOC}"
## Load tester records
epicsEnvSet("TSEL_REF", "$(PREFIX)TimeRef")
dbLoadRecords("esster-extclient.template", "P=$(PREFIX), R=, TSEL_REF=$(TSEL_REF)")

iocInit

