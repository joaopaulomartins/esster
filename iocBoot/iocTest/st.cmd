#!../../bin/linux-x86_64-debug/essterIoc

#- You may have to change essterIoc to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/essterIoc.dbd"
essterIoc_registerRecordDeviceDriver pdbbase

# set PREFIX
epicsEnvSet("PREFIX", "TEST:LEADER:")

## Load ESSTER GENERATOR records
dbLoadRecords("db/essTimeRefGenerator.template", "P=$(PREFIX), R=")

cd "${TOP}/iocBoot/${IOC}"

## Load tester records
epicsEnvSet("PREFIX_INT", "TEST:LOCAL:")
dbLoadRecords("${TOP}/db/essterIocTest-setpoints.template", "P=$(PREFIX), R=, ESSTER=$(PREFIX), TIMESTAMP_REC=$(PREFIX)ExampleTimeSource")
dbLoadRecords("${TOP}/db/essterIocTest.template", "P=$(PREFIX_INT), R=, ESSTER=$(PREFIX)")

iocInit

